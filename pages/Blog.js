import Header from "../pages-section/Header";
import Breadcrumbs from "../pages-section/Breadcrumbs";
import BlogSection from "../pages-section/BlogSection/BlogSection";
import Footer from "../pages-section/Footer";

function Blog() {
  return (
    <>
      <Breadcrumbs/>
      <BlogSection/>
    </>
  ) 
}

export default Blog
