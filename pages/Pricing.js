import Breadcrumbs from "../pages-section/Breadcrumbs";
import PricingSection from "../pages-section/PricingSection/PricingSection";


function Pricing() {
  return (
    <>
         <Breadcrumbs/>
         <PricingSection/>
    </>
  )
}

export default Pricing