import Breadcrumbs from "../pages-section/Breadcrumbs";
import ServicesSection from "../pages-section/ServicesSection/ServicesSection";
import SkillsSection from "../pages-section/SkillsSection";

function About() {
  return (
    <>
        <Breadcrumbs/>
        <ServicesSection/>
        <SkillsSection/>
    </>
  )
}

export default About