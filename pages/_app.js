import Layout from "../pages-section/Layout/Layout";
import '../public/assets/vendor/animate.css/animate.min.css';
import '../public/assets/vendor/bootstrap/css/bootstrap.min.css';
import '../public/assets/vendor/bootstrap-icons/bootstrap-icons.css';
import '../public/assets/vendor/boxicons/css/boxicons.min.css';
import '../public/assets/vendor/glightbox/css/glightbox.min.css';
import '../public/assets/vendor/swiper/swiper-bundle.min.css';
import '../public/assets/css/style.css';

// import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
  return(
    <>
  <Layout>
     <main id="main">
      <Component {...pageProps} />
    </main>
            <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js" strategy="lazyOnload" />
            <script src="assets/vendor/glightbox/js/glightbox.min.js" strategy="lazyOnload" />
            <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js" strategy="lazyOnload" />
            <script src="assets/vendor/php-email-form/validate.js" strategy="lazyOnload" />
            <script src="assets/vendor/purecounter/purecounter.js" strategy="lazyOnload" />
            <script src="assets/vendor/swiper/swiper-bundle.min.js" strategy="lazyOnload" />
            <script src="assets/vendor/waypoints/noframework.waypoints.js" strategy="lazyOnload" />
            <script src="assets/js/main.js" strategy="lazyOnload" />
  </Layout>
  </>
  );
}

export default MyApp
