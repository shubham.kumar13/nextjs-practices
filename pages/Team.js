import Breadcrumbs from "../pages-section/Breadcrumbs";
// import TeamSection from "../pages-section/TeamSection/TeamSection";
import dynamic from 'next/dynamic';
import TeamLoader from "../pages-section/TeamSection/TeamMember/TeamLoader"
import 'react-loading-skeleton/dist/skeleton.css'

const TeamSection = dynamic(() => import('../pages-section/TeamSection/TeamSection'),
{ loading: () => <TeamLoader/>, ssr:false, });

function Team() {
  return (
    <>
    <Breadcrumbs/>
    <TeamSection/>
    </>
  );
}

export default Team