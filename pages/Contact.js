import Breadcrumbs from "../pages-section/Breadcrumbs";
import ContactSection from "../pages-section/ContactSection";


function Contact() {
  return (
    <>
    <Breadcrumbs/>
    <ContactSection/>
    </>
  )
}

export default Contact