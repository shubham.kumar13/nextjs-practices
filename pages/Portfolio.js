import Breadcrumbs from "../pages-section/Breadcrumbs";
import PortfolioSection from "../pages-section/PortfolioSection/PortfolioSection";
import ClientsSection from "../pages-section/ClientsSection";


function About() {
  return (
    <>
    <Breadcrumbs/>
    <PortfolioSection/>
    <ClientsSection/>
    </>
  )
}

export default About