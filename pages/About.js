import Breadcrumbs from "../pages-section/Breadcrumbs";
import AboutSection from "../pages-section/AboutSection";
import Counts from "../pages-section/Counts";
import ClientsSection from "../pages-section/ClientsSection";
import TestimonialsSection from "../pages-section/TestimonialsSection/TestimonialsSection";

function About() {
  return (
    <>
      <Breadcrumbs/>
      <AboutSection/>
      <Counts/>
      <ClientsSection/>
      <TestimonialsSection/>
    </>
  )
}

export default About