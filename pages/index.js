import React from "react";
import HeroSection from "../pages-section/HeroSection";
import FeaturedSection from "../pages-section/FeaturedSection";
import AboutSection from "../pages-section/AboutSection";
import ServicesSection from "../pages-section/ServicesSection";
import ClientsSection from "../pages-section/ClientsSection";
function Home() {
  return (
    <>
    <HeroSection/>
      <FeaturedSection/>
      <AboutSection/>
      <ServicesSection/>
      <ClientsSection/>
    </>
  )
}

export default Home