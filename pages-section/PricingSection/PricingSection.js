import PriceCard from "../PricingSection/PriceCard/PriceCard"

const Pricingcontent =[
  {
    title: "Free",
    price: "$0",
    time: "per month"
  },
  {
    title: "Business",
    price: "$29",
    time: "per month"
  },
  {
    title: "Developer",
    price: "$49",
    time: "per month"
  }
]
function PricingSection() {
  return (
    <section id="pricing" className="pricing">
        <div className="container">
          <div className="row no-gutters">
            {Pricingcontent.map((cardData)=>(
              <PriceCard {...cardData}/>
            ))}
          </div>
        </div>
      </section>
  ); 
}

export default PricingSection