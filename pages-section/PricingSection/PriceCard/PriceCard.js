
function PriceCard() {
  return (
      <div className="col-lg-4 box">
      <h3>Developer</h3>
      <h4>$49<span>per month</span></h4>
      <ul>
        <li><i className="bx bx-check" /> Quam adipiscing vitae proin</li>
        <li><i className="bx bx-check" /> Nec feugiat nisl pretium</li>
        <li><i className="bx bx-check" /> Nulla at volutpat diam uteera</li>
        <li><i className="bx bx-check" /> Pharetra massa massa ultricies</li>
        <li><i className="bx bx-check" /> Massa ultricies mi quis hendrerit</li>
      </ul>
      <a href="#" className="buy-btn">Buy Now</a>
    </div>
  );
}

export default PriceCard