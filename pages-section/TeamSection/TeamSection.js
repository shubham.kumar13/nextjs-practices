import TeamMember from "./TeamMember/TeamMember";

const teamMembers = [
  {
    image: "assets/img/team/team-1.jpg",
    name: "Walter White",
    designation: "Chiszdfsive Officer",
    description: "Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut"
  },
  {
    image: "assets/img/team/team-2.jpg",
    name: "Sarah Jhinson",
    designation: "Product Manager",
    description: "Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut"
  },
  {
    image: "assets/img/team/team-3.jpg",
    name: "William Anderson",
    designation: "CTO",
    description: "Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut"
  },
  {
    image: "assets/img/team/team-2.jpg",
    name: "Walter White",
    designation: "Chief Executive Officer",
    description: "Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut"
  },
];

function TeamSection() {
  return (
    <section id="team" className="team">
        <div className="container">
          <div className="row">
          {teamMembers.map((memberDetails) => (
          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <TeamMember {...memberDetails}/>
          </div>
          ))}
        </div>
      </div>
    </section>
  );
}

export default TeamSection