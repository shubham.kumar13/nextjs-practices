import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

function TeamLoader() {
    return (
        <div className="container">
        <div className="row my-4">
        {[1, 2, 3, 4].map(() => (
          <div className="col-sm-6 col-lg-3">
            <Skeleton height={300} />
          </div>
        ))}
      </div>
      </div>
    );
  }
  export default TeamLoader


