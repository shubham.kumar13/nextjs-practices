
function TeamMember({name, designation, image, description}) {
    return (
        <div className="member">
          <img src={image} alt="" />
          <h4>{name}</h4>
          <span>{designation}</span>
          <p>{description}</p>
          <div className="social">
              <a href><i className="bi bi-twitter" /></a>
              <a href><i className="bi bi-facebook" /></a>
              <a href><i className="bi bi-instagram" /></a>
              <a href><i className="bi bi-linkedin" /></a>
          </div>
        </div>
    );
  }
  export default TeamMember