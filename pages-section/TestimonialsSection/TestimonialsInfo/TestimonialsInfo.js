
function TestimonialsInfo({ name, designation, image, description}) {
  return (
    <div className="col-lg-6">
      <div className="testimonial-item mt-4">
        <img src={image} className="testimonial-img" alt="" />
        <h3>{name}</h3>
        <h4>{designation}</h4>
        <p>
          <i className="bx bxs-quote-alt-left quote-icon-left" />
          {description}
          <i className="bx bxs-quote-alt-right quote-icon-right" />
        </p>
      </div>
    </div>
  );
}

export default TestimonialsInfo