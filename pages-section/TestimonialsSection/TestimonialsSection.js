import TestimonialsInfo from "../TestimonialsSection/TestimonialsInfo/TestimonialsInfo"

const TestimonilaContent = [
  {
    name: "Saul Goodman",
    designation: "Ceo & Founder",
    image: "assets/img/testimonials/testimonials-1.jpg",
    description: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper."
  },
  {
    name: "Sara Wilsson",
    designation: "Designer",
    image: "assets/img/testimonials/testimonials-2.jpg",
    description: "Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa."
  },
  {
    name: "Jena Karlis",
    designation: "Store Owner",
    image: "assets/img/testimonials/testimonials-3.jpg",
    description: "Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim."
  },
  {
    name: "Matt Brandon",
    designation: "Freelancer",
    image: "assets/img/testimonials/testimonials-4.jpg",
    description: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper."
  },
  {
    name: "John Larson",
    designation: "Entrepreneur",
    image: "assets/img/testimonials/testimonials-5.jpg",
    description: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper."
  },
  {
    name: "Emily Harison",
    designation: "Store Owner",
    image: "assets/img/testimonials/testimonials-6.jpg",
    description: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper."
  },
]
function TestimonialsSection() {
  return (
    <section id="testimonials" className="testimonials">
    <div className="container">
      <div className="section-title">
        <h2>Testimonials</h2>
        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>
      <div className="row">
        {TestimonilaContent.map((TestimonilaData)=>(
          <TestimonialsInfo {...TestimonilaData}/>
        ))}
      </div>
    </div>
  </section>
  ); 
}

export default TestimonialsSection