
function BlogRecentPosts({title, date, image}) {
    return (
     <div className="post-item clearfix">
        <img src={image} alt="" />
        <h4><a href="blog-single.html">{title}</a></h4>
        <time dateTime="2020-01-01">{date}</time>
      </div>
    );
  }
  
  export default BlogRecentPosts