
function BlogEntry({title, image, decription}) {
  return (
        <article className="entry">
          <div className="entry-img">
            <img src={image} alt="" className="img-fluid" />
          </div>
          <h2 className="entry-title">
            <a href="blog-single.html">{title}</a>
          </h2>
          <div className="entry-meta">
            <ul>
              <li className="d-flex align-items-center"><i className="bi bi-person" /> <a href="blog-single.html">John Doe</a></li>
              <li className="d-flex align-items-center"><i className="bi bi-clock" /> <a href="blog-single.html"><time dateTime="2020-01-01">Jan 1, 2020</time></a></li>
              <li className="d-flex align-items-center"><i className="bi bi-chat-dots" /> <a href="blog-single.html">12 Comments</a></li>
            </ul>
          </div>
          <div className="entry-content">
            <p>
              {decription}
            </p>
            <div className="read-more">
              <a href="blog-single.html">Read More</a>
            </div>
          </div>
        </article>
      );
}

export default BlogEntry