import PortfolioInfo from "../PortfolioSection/PortfolioInfo/PortfolioInfo"

const Portfoliocontent = [
  {
    name : "App",
    title: "App 1",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-1.jpg",
    category: "filter-app"
  },
  {
    name : "Web 3",
    title: "Web",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-2.jpg",
    category: "filter-web"
  },
  {
    name : "App",
    title: "App 2",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-3.jpg",
    category: "filter-app"    
  },
  {
    name : "App",
    title: "App 3",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-4.jpg",
    category: "filter-app"
  },
  {
    name : "Web 1",
    title: "Web",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-5.jpg",
    category: "filter-web"
  },
  {
    name : "Web 2",
    title: "Web",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-6.jpg",
    category: "col-lg-4 col-md-6 portfolio-item filter-web"
  },
  {
    name : "Card 1",
    title: "Card",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-7.jpg",
    category: "col-lg-4 col-md-6 portfolio-item filter-card"
  },
  {
    name : "Card 2",
    title: "Card",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-8.jpg",
    category: "filter-card"
  },
  {
    name : "Card 3",
    title: "Card",
    link: "portfolio-details.html",
    image: "assets/img/portfolio/portfolio-9.jpg",
    category: "filter-card"
  }
]
function PortfolioSection() {
  return (
    <section id="portfolio" className="portfolio">
    <div className="container">
      <div className="row">
        <div className="col-lg-12 d-flex justify-content-center">
          <ul id="portfolio-flters">
            <li data-filter="*" className="filter-active">All</li>
            <li data-filter=".filter-app">App</li>
            <li data-filter=".filter-card">Card</li>
            <li data-filter=".filter-web">Web</li>
          </ul>
        </div>
      </div>
      <div className="row portfolio-container">
        {Portfoliocontent.map((PortfolioData)=>(
          <PortfolioInfo {...PortfolioData}/>
        ))}
      </div>
    </div>
  </section>
    );
}

export default PortfolioSection