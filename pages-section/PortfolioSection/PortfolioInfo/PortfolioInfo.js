function PortfolioInfo({name, title, link, image, category}) {
  return (
    <div className="col-lg-4 col-md-6 portfolio-item filter-app" >
    <div className="portfolio-wrap">
      <img src={image} className="img-fluid" alt="" />
      <div className="portfolio-info">
        <h4>{title}</h4>
        <p>{name}</p>
        <div className="portfolio-links">
          <a href={image} data-gallery="portfolioGallery" className="portfolio-lightbox" title={title}><i className="bx bx-plus" /></a>
          <a href={link} title="More Details"><i className="bx bx-link" /></a>
        </div>
      </div>
    </div>
  </div>
  );
}

export default PortfolioInfo