import ServicesInfo from "../ServicesSection/ServicesInfo/ServicesInfo"

const ServicesContent = [
  {
    service: "Lorem Ipsum",
    description: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    image: "bx bxl-dribbble"
  },
  {
    service: "Sed ut perspiciatis",
    description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore",
    image: "bx bx-fi"
  },
  {
    service: "Magni Dolores",
    description: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
    image: "bx bx-tachometer"
  },
  {
    service: "Nemo Enim",
    description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis",
    image: "bx bx-world"
  },
  {
    service: "Dele cardo",
    description: "Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur",
    image: "bx bx-slideshow"
  },
  {
    service: "Divera don",
    description: "Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur",
    image: "bx bx-arch"
  }
]


function ServicesSection() {
  return (
    <section id="services" className="services">
    <div className="container">
      <div className="row">
        {ServicesContent.map((servicesInfo)=> (
          <div className="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
          <ServicesInfo {...servicesInfo} />
          </div>
        ))}
      </div>
    </div>
  </section>
  );
}

export default ServicesSection