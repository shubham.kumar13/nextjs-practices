import React from "react";
import Header from "../Header";
import Footer from "../Footer";

function Home({children}) {
  return (
    <>
    <Header/>
    {children}
    <Footer/>
    </>
  );
}

export default Home